'use strict';

angular
    .module('system', [
        'ui.router',
        'ngSanitize',
        'system.config',
        'system.services',
        'satellizer'
    ])
    .config(['$stateProvider', '$urlRouterProvider', '$authProvider', function($stateProvider, $urlRouterProvider, $authProvider) {

        $authProvider.facebook({
            clientId: '1853408908228730',
            responseType: 'token'
        });

        $authProvider.google({
            clientId: '82632221314-34rcc2dn898365epoen6vocamsghgmsf.apps.googleusercontent.com',
            redirectUri: window.location.origin + '/success',
            responseType: 'token',
            scope: ['email'],
            popupOptions: { width: 500, height: 500 }
        });

        $stateProvider
            .state('login', {
                url: '/login',
                template: '<login></login>'
            })
            .state('lobby', {
                url: '/lobby',
                template: '<lobby></lobby>',
                loginRequired: true
            })
            .state('game', {
                url: '/game/{gameId}/player/{playerId}',
                template: '<game></game>',
                loginRequired: true
            })
            .state('rules', {
                url: '/rules',
                templateUrl: 'rules'
            })
            .state('help', {
                url: '/help',
                templateUrl: 'help'
            })
            .state('error', {
                url: '/error',
                templateUrl: 'error'
            });

        $urlRouterProvider.when('', '/login');
        $urlRouterProvider.when('/', '/login');
        $urlRouterProvider.otherwise('/error');

    }])
    .run(['$location', '$rootScope', 'AuthService', function ($location, $rootScope, AuthService) {
        console.log('System fired');

        var previous;

        $rootScope.lobby = function() {
            $location.path('/lobby');
        };

        $rootScope.logout = function() {
            $rootScope.username = '';
            AuthService.logout();
            $location.path("/");
        };

        $rootScope.rules = function() {
            $location.path('/rules');
        };

        $rootScope.help = function() {
            $location.path('/help');
        };

        $rootScope.$on('$stateChangeStart', function (event, next, current) {
            console.log('$stateChangeStart fired');
            if (next.loginRequired && !AuthService.isAuthenticated()) {
                previous = $location.path();
                $location.path('/login').replace();
            } else if (previous && AuthService.isAuthenticated()) {
                $location.path(previous).replace();
                previous = null;
            }
        });

        $rootScope.updateAuthentication = function() {
            if(AuthService.isAuthenticated()) {
                AuthService.getUsername().then(
                    function successCallback(response) {
                        $rootScope.username = response.data.emails[0].value;
                    },
                    function errorCallback(response) {
                        console.error('Could not access authenticated user\'s email: ' + response.statusText);
                        $rootScope.logout(); // you may have an expired authentication token
                    }
                );
            }
        };

        $rootScope.isAuthenticated = function(){
            return AuthService.isAuthenticated();
        };

        $rootScope.updateAuthentication();

    }]);