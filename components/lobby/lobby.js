'use strict';

angular
    .module('system')
    .component('lobby', {
        templateUrl: 'components/lobby/lobby.html',
        controller: 'LobbyController'
    })
    .controller('LobbyController', ['$rootScope', '$scope', 'AuthService', 'AlertService', 'GameService', '$location', function($rootScope, $scope, AuthService, AlertService, GameService, $location) {
        console.log("LobbyController fired");
        $scope.alerts = [];
        $scope.availableGames = [];
        var socket;

        function initialize() {
            console.log('lobby socket: initializing');

            socket = io.connect('/lobby');

            console.log(socket);

            socket.on('updateGameList', function(gameList) {
                console.log('lobby socket: updating game list');
                $scope.availableGames = gameList;
                $scope.$apply();
            });

            socket.on('reconnect', function() {
                initialize();
            })

            socket.emit('test');
        }

        $scope.createGame = function() {
            GameService.createGame($scope.GameName).then(
                function successCallback(response) {
                    console.log('game created: ' + response.data);
                    $scope.joinGame(response.data.gameId);
                },
                function errorCallback(response){
                    console.log('unable to create game: ' + response.statusText);
                }
            )
        };
        $scope.joinGame = function(gameId) {
            GameService.joinGame(gameId, $rootScope.username).then(
                function successCallback(response) {
                    console.log('game joined: ' + response.data);
                    $location.path("/game/"+ response.data.gameId + "/player/" + response.data.playerId);
                },
                function errorCallback(response) {
                    console.log('unable to join game: ' + response.statusText);
                    $scope.alerts.push(AlertService.danger("Unsuccessful!", "You were not able to join game " + gameId));
                }
            )
        };

        initialize();

    }])