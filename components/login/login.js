'use strict';

angular
    .module('system')
    .component('login', {
        templateUrl: 'components/login/login.html',
        controller: 'LoginController'
    })
    .controller('LoginController', ['$auth', '$scope', 'AuthService', '$location', 'AlertService', '$rootScope', function($auth, $scope, AuthService, $location, AlertService, $rootScope) {
        console.log("LoginController fired");
        $scope.alerts = [];

        if(AuthService.isAuthenticated()) {
            $scope.alerts.push(AlertService.success("Success!", "You are logged in!"));
            $location.path('/lobby');
        }

        $scope.login = function() {
            if(AuthService.login($scope.username, sha256($scope.password))) {
                $scope.alerts.push(AlertService.success("Success!", "You have logged in!"));
                AuthService.setAuthentication();
                $rootScope.username = $scope.username;
                $location.path('/lobby');
            }
            else
                $scope.alerts.push(AlertService.info("Note:", "Local login is disabled except for administrator access. Please log in with a third-party authenticator"));
        }

        $scope.authenticate = function(provider) {
            console.log('logging in with ' + provider);
            var name = provider.charAt(0).toUpperCase() + provider.slice(1);
            AuthService.authenticate(provider).then(
                function successCallback(response) {
                    $scope.alerts.push(AlertService.success("Success!", "You have logged in through " + name + "!"));
                    $rootScope.updateAuthentication();
                    $location.path('/lobby');
                },
                function errorCallback(response) {
                    console.log(response);
                    $scope.alerts.push(AlertService.danger("Unsuccessful!", "We were not able to authenticate you through " + name));
                }
            )
        };

    }])