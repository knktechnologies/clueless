'use strict';

angular
    .module('system')
    .component('game', {
        templateUrl: 'components/game/game.html',
        controller: 'GameController'
    })
    .controller('GameController', ['$scope', '$auth', 'AlertService', 'GameService', '$stateParams', '$location', '$timeout', function($scope, $auth, AlertService, GameService, $stateParams, $location, $timeout) {
        console.log("GameController fired");
        $scope.alerts = [];
        $scope.gameStarted = false;
        $scope.master = false;
        $scope.gameboard = {};
        $scope.location = {};
        $scope.moves = [];
        $scope.players = [];
        $scope.character = {};
        $scope.accuseOnly = false;
        $scope.cards = [];
        $scope.suggestion = {
            character:  {},
            room:       {},
            weapon:     {}
        };
        $scope.disprove_card = {};
        var socket;

        $scope.submitAccusation = function() {
            console.log('submitting accusation');
            console.log($scope.suggestion.character + ', ' + $scope.suggestion.room + ', ' + $scope.suggestion.weapon);
            if ($scope.suggestion.character.length || $scope.suggestion.room.length || $scope.suggestion.weapon.length) {
                socket.emit('makeAccusation', getCardIndex($scope.suggestion.character), getCardIndex($scope.suggestion.room), getCardIndex($scope.suggestion.weapon));
            }
            else {
                $scope.alerts.push(AlertService.danger("Incomplete!", "You must submit all fields for an accusation"));
                $timeout(function(){ $scope.alerts.pop(); }, 5000);
            }
        };
        $scope.submitSuggestion = function() {
            console.log('submitting suggestion');
            console.log($scope.suggestion.character + ', ' + $scope.suggestion.room + ', ' + $scope.suggestion.weapon);
            console.log($scope.suggestion);
            if ($scope.suggestion.character || $scope.suggestion.room || $scope.suggestion.weapon) {
                if ($scope.location == $scope.gameboard.deck[getCardIndex($scope.suggestion.room)].location) {
                    socket.emit('makeSuggestion', getCardIndex($scope.suggestion.character), getCardIndex($scope.suggestion.room), getCardIndex($scope.suggestion.weapon));
                    $scope.enableSuggestion = false;
                    $scope.enableSpecialSuggestion = false;
                }
                else {
                    $scope.alerts.push(AlertService.danger("Illegal move!", "You can only suggest the room you currently occupy"));
                    $timeout(function(){ $scope.alerts.pop(); }, 5000);
                }
            }
            else {
                $scope.alerts.push(AlertService.danger("Incomplete!", "You must submit all fields for a suggestion"));
                $timeout(function(){ $scope.alerts.pop(); }, 5000);
            }
        };
        $scope.submitDisprove = function() {
            console.log('enableSuggestion: ' + $scope.enableSuggestion + ', enableDisprove: ' + $scope.enableDisprove + ', enableTurn: ' + $scope.enableTurn);
            console.log('submitting disproval');
            console.log($scope.disprove_card);
            socket.emit('submitDisprove', $scope.disprove_card);
        };
        $scope.quitGame = function() {
            socket.emit('quit');
        };
        $scope.locatePlayers = function(location) {
            var players = [];
            $scope.players.forEach(function(player) {
                if(player.location == location)
                    players.push(player)
            });

            return players;
        };
        $scope.getAvailableMoves = function() {
            return GameService.getMoves($scope.location);
        };
        $scope.gameReady = function() {
            if($scope.master && !$scope.gameStarted)
                return true;
            else
                return false
        };
        $scope.startGame = function() {
            if(getPlayerCount() > 2)
                socket.emit('startGame');
            else {
                $scope.alerts.push(AlertService.danger("Not yet!", "You need at least three players to start"));
                $timeout(function () {
                    $scope.alerts.pop();
                }, 5000);
            }
        };
        $scope.nextTurn = function() {
            $scope.enableSuggestion = false;
            socket.emit('setNextTurn');
            console.log('enableSuggestion: ' + $scope.enableSuggestion + ', enableDisprove: ' + $scope.enableDisprove + ', enableTurn: ' + $scope.enableTurn);
        };
        $scope.pass = function() {
            $scope.accuseOnly = false;
            $scope.enableSuggestion = false;
            $scope.enableSpecialSuggestion = false;
            if($scope.alerts.length)
                $scope.alerts.slice(0,$scope.alerts.length);
            socket.emit('passNextTurn');
        };
        $scope.moveUp = function() {
            if($scope.locatePlayers($scope.location-7).length && GameService.isHall($scope.location-7)) {
                $scope.alerts.push(AlertService.danger("Invalid move!", "Only one person per hall"));
                $timeout(function () {
                    $scope.alerts.pop();
                }, 5000);
            }
            else {
                socket.emit('moveUp');
                $scope.enableSuggestion = false;
                $scope.enableSpecialSuggestion = false;
            }
        };
        $scope.moveDown = function() {
            if($scope.locatePlayers($scope.location+7).length && GameService.isHall($scope.location+7)) {
                $scope.alerts.push(AlertService.danger("Invalid move!", "Only one person per hall"));
                $timeout(function () {
                    $scope.alerts.pop();
                }, 5000);
            }
            else {
                socket.emit('moveDown');
                $scope.enableSuggestion = false;
                $scope.enableSpecialSuggestion = false;
            }
        };
        $scope.moveRight = function() {
            if($scope.locatePlayers($scope.location+1).length && GameService.isHall($scope.location+1)) {
                $scope.alerts.push(AlertService.danger("Invalid move!", "Only one person per hall"));
                $timeout(function () {
                    $scope.alerts.pop();
                }, 5000);
            }
            else {
                socket.emit('moveRight');
                $scope.enableSuggestion = false;
                $scope.enableSpecialSuggestion = false;
            }
        };
        $scope.moveLeft = function() {
            if($scope.locatePlayers($scope.location-1).length && GameService.isHall($scope.location-1)) {
                $scope.alerts.push(AlertService.danger("Invalid move!", "Only one person per hall"));
                $timeout(function () {
                    $scope.alerts.pop();
                }, 5000);
            }
            else {
                socket.emit('moveLeft');
                $scope.enableSuggestion = false;
                $scope.enableSpecialSuggestion = false;
            }
        };
        $scope.moveDiagonal = function() {
            socket.emit('moveDiagonal');
            $scope.enableSuggestion = false;
            $scope.enableSpecialSuggestion = false;
        };
        $scope.filterDisprovalCards = function() {
            return GameService.getDisprovalCards($scope.cards,$scope.suggestion);
        };

        $scope.enableTurn = false;
        $scope.enableSuggestion = false;
        $scope.enableSpecialSuggestion = false;
        $scope.enableDisprove = false;

        var updateGame = function(game) {
            console.log('UPDATE: enableTurn: ' + $scope.enableTurn + ', enableSuggestion: ' + $scope.enableSuggestion + ', enableDisprove: ' + $scope.enableDisprove);
            $scope.enableDisprove = game.enableDisprove;
            $scope.players = game.players;
            $scope.gameStarted = game.isStarted;
            $scope.gameName = game.gameName;
            $scope.suggestion = game.suggestion;
            //if($scope.enableDisprove && disprovalCardsEmpty())
            //    $scope.pass();
            $scope.$apply();
        };
        var initialize = function() {
            console.log('game socket: initializing');

            socket = io.connect('/game');

            socket.on('connect', function() {
                console.log('game socket: authenticating with credentials');
                socket.emit('authenticate', { gameId: $stateParams.gameId, playerId: $stateParams.playerId });
            });

            socket.on('updateGame', function(game) {
                console.log('game socket: updating game');
                updateGame(game);
                $scope.$apply();
            });

            socket.on('updatePlayer', function(player) {
                $scope.character = player.character;
                $scope.cards = player.cards;
                $scope.location = player.location;
                $scope.moves = $scope.getAvailableMoves();
                $scope.$apply();
            });

            socket.on('makeMaster', function() {
                $scope.master = true;
                $scope.$apply();
            });

            socket.on('enableTurn', function(player) {
                $scope.enableTurn = true;
                $scope.$apply();
            });

            socket.on('disableTurn', function() {
                $scope.enableTurn = false;
                $scope.$apply();
            });

            socket.on('completeTurn', function() {
                $scope.nextTurn();
            });

            socket.on('enableSuggestion', function() {
                $scope.enableSuggestion = true;
                $scope.$apply();
            });

            socket.on('enableSpecialSuggestion', function() {
                $scope.enableSpecialSuggestion = true;
                $scope.$apply();
            });

            socket.on('accuseOnly', function() {
                $scope.accuseOnly = true;
                $scope.$apply();
            });

            socket.on('showDisprove', function(proof) {
                console.log('your suggestion has been disproved with ' + proof);
                $scope.alerts.push(AlertService.info("Clue!", "Your sugggestion has been disproved with " + proof));
                $scope.$apply();
                $timeout(function(){ $scope.alerts.pop(); $scope.$apply();}, 5000);
            });

            socket.on('gameError', function(errorMsg) {
                console.log('game error');
                $scope.alerts = AlertService.warning(errorMsg);
            });

            socket.on('quit', function() {
                $location.path('/lobby');
            });
        };
        var createGameboard = function() {
            GameService.getClues().then(
                function successCallback(response) {
                    $scope.gameboard = GameService.createGameboard(response.data);
                    startingPositions();
                },
                function errorCallback(response) {
                    console.error('Failed to get clues: ' + response.statusText)
                }
            )
        };
        var startingPositions = function() {
            var players = [];
            $scope.gameboard.characters.forEach(function(player) {
                $scope.players.push({location:player.start, character:player.index});
            })
        };
        var getCardIndex = function(name) {
            var i;
            for(i=0; i< $scope.gameboard.deck.length; i++) {
                if($scope.gameboard.deck[i].name == name)
                    return i;
            }
        };
        var getCardName = function(index) {
            return $scope.gameboard.deck[index].name;
        };
        var getPlayerCount = function() {
            var count = 0;
            $scope.players.forEach(function(player){
                if(player.playerId)
                    count++;
            });
            return count;
        };

        initialize();
        createGameboard();

    }]);
