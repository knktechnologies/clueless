'use strict';

angular
    .module('system')
    .component('chat', {
        templateUrl: 'components/chat/chat.html',
        controller: 'ChatController'
    })
    .controller('ChatController', ['$scope', '$stateParams', '$rootScope', function($scope, $stateParams, $rootScope) {
        console.log("ChatController fired");
        var socket;
        $scope.messages = [];

        var initialize = function() {
            console.log('chat socket: initializing');

            socket = io.connect('/chat');

            socket.on('requestCredentials', function() {
                console.log('chat socket: authenticating with credentials');
                socket.emit('authenticate', { gameId: $stateParams.gameId, playerId: $stateParams.playerId, username: $rootScope.username }); // fifth
            });

            socket.on('updateChat', function(data) {
                $scope.messages = data;
                $scope.$apply();
            });
        };

        $scope.postMessage = function() {
            console.log('post message: ' + $scope.message);
            socket.emit('postMessage', { gameId: $stateParams.gameId, playerId: $stateParams.playerId, content: $scope.message });
            $scope.message = '';
        };

        $scope.getStyle = function(message) {
            if(message.username == $rootScope.username)
                return 'success';
            else if(message.username == 'system')
                return 'default';
            else
                return 'primary';
        };

        initialize();

    }]);
