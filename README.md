# Clue-Less
This is the master branch for the Clue-Less game program. A baseline Node file structure has been committed, but the structure is not meant to be followed. It's simply a template.

All of the commands I'll have below are for a Unix command line. You may change this file where it can be deemed helpful.

## Development operations
* You'll need to be able to access GitLab (pull/push) securely, so [set up an SSH key](https://docs.gitlab.com/ce/ssh/README.html)

* Next, find a location on your drive and establish that as your git repository (e.g. Desktop): ``` git init ```

* Clone this directory: ``` git clone git@gitlab.com:nullterminators/clue-less.git ```

* Install all of the standard dependencies: ``` npm install ```

* Create and switch to your own local branch: ``` git checkout -b <yournewbranchname> ```

* Make changes and see the changes you've made: ``` git status ```
 
* Bundle your changes to be committed (this is saved locally): ``` git add ./ ```

* Commit the changes you've made (this is saved locally): ``` git commit -m '<description of changes>' ```

* Push those changes to the git repository: ``` git push --set-upstream git@gitlab.com:nullterminators/clue-less.git <yournewbranchname> ```
 

## Notes
* Add any new dependencies to the ``` package.json ``` file (whenever you add a new library using npm, you can automatically add it as a dependency using the '--save' option, e.g. ``` npm install jquery --save ```)

* For files you don't want to include on your commits, add the file(s) to ``` .gitignore ``` file

* For Facebook to permit you to use their authentication system, you need to append the following line to ``` /etc/hosts ``` : ``` 127.0.0.1 clue-less.development.com ```

* If you don't want to use a third-party authenticator to keep a persistent login, you may use the following credentials: username: ``` admin ```, password: ``` password ``` (Note: this is not a persistent login and will be lost when you refresh page)