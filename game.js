var _ = require('underscore');
var config = require('./config.js');
var clues = require('./res/data/clues.json');
var deck = clues.rooms.concat(clues.characters.concat(clues.weapons)); // this is used as the universal reference

/*
This is what a game object should like...

// on updateGame -- broadcasted to everyone
gameList: [
{
        gameId: 'something',
        gameName: 'something',
        players: [
            {
                username: 'username',
                playerId: '1241414sd'       // filtered
                cards: [],                  // added after start // filtered
                character: 0-5              // added after start
            }
        ],
        isStarted: T/F,
        inSuggestion: T/F,                  // added after start
        playerTurn: 0-6,                    // added after start
        maxPlayers: 6,
        minPlayers: 2,
        confidentialFile: [3];                        // taken out in format
    }
]
*/

var gameList = [];

function assignCharacters(players)
{
    var arr = [0, 1, 2, 3, 4, 5];
    var shuffledArray = _.shuffle(arr);
    for(var i=0; i< shuffledArray.length; i++) {
        if(!players[i])
            players.push({cards: [], character: shuffledArray[i], location: clues.characters[shuffledArray[i]].start});
        else {
            players[i].character = shuffledArray[i];
            players[i].location = clues.characters[shuffledArray[i]].start
        }
    }
}

function addGame(game) {
    game.players = [];
    game.isStarted = false;
    game.enableDisprove = false;
    game.suggestion = { character:{}, room: {}, weapon:{} };
    game.suggester = '';
    game.minPlayers = config.minPlayers;
    game.maxPlayers = config.maxPlayers;
    game.currentTurn = 0;
    game.secret = [];
    gameList.push(game);
    return formatGame(game.gameId);
}

function dealCards(game) {
    // creates a deck by concatinating all arrays
    var temp_deck = clues.rooms.concat(clues.characters.concat(clues.weapons));

    // removes confidential files from it
    temp_deck.splice(game.confidentialFile[0],1);
    temp_deck.splice(game.confidentialFile[1],1);
    temp_deck.splice(game.confidentialFile[2]-2,1);

    temp_deck = _.shuffle(temp_deck);

    for(i = 0; i < temp_deck.length; i++)
        getNextPlayer(game).cards.push(temp_deck[i].i);
}

function departGame(gameId, playerId) {
    var game = getGame(gameId);
    if(game){
        var departingPlayer = getPlayer(gameId, playerId);
        if(departingPlayer.playerId == game.players[game.currentTurn].playerId)
            setNextTurn(gameId);
        departingPlayer.playerId = null;
        departingPlayer.playerName = null;
        if(!game.isStarted)
            removeObject(game.players,departingPlayer);
        if(!playersRemain(gameId)){
            //kill the game
            console.info('game empty -- killing game');
            removeObject(gameList, game);
        }
    }
}

function playersRemain(gameId) {
    var game = getGame(gameId);
    var playersRemain = false;
    game.players.forEach(function(player) {
        if(player.playerId != null)
            playersRemain = true;
    });
    return playersRemain;
}


// returns a formatted game object, private variables excluded
function formatGame(gameId) {
    var formattedGame = {};
    var game = getGame(gameId);
    formattedGame.gameId = game.gameId;
    formattedGame.gameName = game.gameName;
    formattedGame.maxPlayers = game.maxPlayers;
    formattedGame.minPlayers = game.minPlayers;
    formattedGame.isStarted = game.isStarted;
    formattedGame.suggester = game.suggester;
    formattedGame.enableDisprove = game.enableDisprove;
    formattedGame.suggestion = game.suggestion;
    formattedGame.players = new Array(game.players.length);
    formattedGame.playerCount = getPlayerCount(game.players);
    game.players.forEach(function(player) {
        formattedGame.players[game.players.indexOf(player)] = {};
        formattedGame.players[game.players.indexOf(player)].playerName = player.playerName;
        formattedGame.players[game.players.indexOf(player)].character = player.character;
    });
    return formattedGame;
}

// formats a list of games
function formatGames(list) {
    return _.map(list, function(game) {
        return formatGame(game.gameId);
    });
}

// returns all player attributes
function formatPlayer(gameId, playerId) {
    var game = getGame(gameId);
    return _.find(game.players, function(player) { return player.playerId == playerId });
}

function getCharacterName(index) {
    return clues.characters[index].name;
}

function getGame(gameId) {
    var game = _.find(gameList, function(game) { return game.gameId === gameId; }) || undefined;
    return game;
}

function getPlayer(gameId, playerId) {
    var game = _.find(gameList, function(game) { return game.gameId === gameId; }) || undefined;
    var player = _.find(game.players, function(player) { return player.playerId === playerId; }) || undefined;
    return player;
}

function getPlayerCount(players) {
    var count = 0;
    players.forEach(function(player) {
        if(player.playerId)
            count++;
    });
    return count;
}

function getNextPlayer(game) {
    game.currentTurn++;
    if(game.currentTurn == game.players.length)
        game.currentTurn = 0;
    while(!game.players[game.currentTurn].playerId || (!game.players[game.currentTurn].isActive && !game.enableDisprove)) {
        game.currentTurn++;
        if(game.currentTurn == game.players.length)
            game.currentTurn = 0;
    }

    return game.players[game.currentTurn];
}

function isRoom(room) {
    return _.contains(config.rooms, room);
}

function joinGame(game, player) {
    player.cards = [];
    player.character = {};
    player.isActive = true;
    console.info('adding player');
    console.info(player);
    game.players.push(player);
    return {gameId: game.gameId, playerId: player.playerId };
}

function list() {
    return formatGames(_.filter(gameList, function (game) {
        return (game.isStarted == false && game.players.length < config.maxPlayers);
    }));
}

function makeSuggestion(gameId, playerId, character, room, weapon) {
    var game = getGame(gameId);
    game.suggestion={character:character, room:room, weapon:weapon};
    game.enableDisprove = true;
    game.suggester = playerId;
    return getNextPlayer(game);
}

function makeAccusation(gameId, playerId, character, room, weapon) {
    var game = getGame(gameId);
    console.info('ACCUSATION MADE: ' + character + ',' + room + ',' + weapon);
    console.info('comparing it to: ' + game.confidentialFile[0] + ', ' + game.confidentialFile[1] + ',' + game.confidentialFile[2]);
    return ((character == game.confidentialFile[0]) && (room == game.confidentialFile[1]) && (weapon == game.confidentialFile[2]))
}

function disprove(gameId, playerId) {
    var game = getGame(gameId);
    game.enableDisprove = false;
    var currentPlayerId = getNextPlayer(game).playerId;
    console.info(game.players[game.currentTurn]);
    while(game.suggester != currentPlayerId) {
        console.info(game.players[game.currentTurn]);
        currentPlayerId = getNextPlayer(game).playerId;
    }
}

function moveCharacterToRoom(gameId, character, room) {
    var game = getGame(gameId);
    var return_player = {};
    game.players.forEach(function(player) {
        console.info('player character is ' + player.character);
        console.info('suggested character is ' + character);
        console.info('this is the supposed character ' + (character - 9));
        if(parseInt(player.character) == (parseInt(character) - 9)) {
            console.info('character ' + player.character + ' has been chosen');
            player.location = clues.rooms[room].location;
            return_player = player;
        }
    });
    return return_player;
}

function playerMove(player, direction) {
    player.location += direction;
    return player;
}

function playerMoveUp(gameId, playerId) {
    return playerMove(getPlayer(gameId, playerId), config.up);
}

function playerMoveDown(gameId, playerId) {
    return playerMove(getPlayer(gameId, playerId), config.down);
}

function playerMoveRight(gameId, playerId) {
    return playerMove(getPlayer(gameId, playerId), config.right);
}

function playerMoveLeft(gameId, playerId) {
    return playerMove(getPlayer(gameId, playerId), config.left);
}

function playerMoveDiagonal(gameId, playerId) {
    var player = getPlayer(gameId, playerId);
    return playerMove(player, config.diagonals[player.location.toString()]);
}

function removeObject(objects, object) {
    var index = objects.indexOf(object);
    if(index !== -1) {
        objects.splice(index, 1);
    }
}

function startGame(game) {
    game.isStarted = true;

    // set playing order by shuffling players
    game.players = _.shuffle(game.players);

    // assign characters to players
    assignCharacters(game.players);


    //assign confidential file
    game.confidentialFile = setConfidentialFile(game);


    //deal cards
    dealCards(game);

    // set the turn
    game.currentTurn = 0;

    return game;
}

function setConfidentialFile(game) {

    var i = 0;
    clues.rooms.forEach(function(room) {room.i = i; i++;});
    clues.characters.forEach(function(character) {character.i = i; i++;});
    clues.weapons.forEach(function(weapon) {weapon.i = i; i++;});

    var shuffledCharacters = _.shuffle(clues.characters);
    var shuffledWeapons = _.shuffle(clues.weapons);
    var shuffledRooms = _.shuffle(clues.rooms);

    return [shuffledCharacters[0].i, shuffledRooms[0].i, shuffledWeapons[0].i];
}

function setNextTurn(gameId) {
    var game = getGame(gameId);
    var nextPlayer = getNextPlayer(game);
}

exports.list = list;
exports.addGame = addGame;
exports.getGame = getGame;
exports.disprove = disprove;
exports.startGame = startGame;
exports.getPlayer = getPlayer;
exports.getCharacterName = getCharacterName;
exports.isRoom = isRoom;
exports.joinGame = joinGame;
exports.makeSuggestion = makeSuggestion;
exports.makeAccusation = makeAccusation;
exports.moveCharacterToRoom = moveCharacterToRoom;
exports.departGame = departGame;
exports.setNextTurn = setNextTurn;
exports.playerMoveUp = playerMoveUp;
exports.playerMoveDown = playerMoveDown;
exports.playerMoveRight = playerMoveRight;
exports.playerMoveLeft = playerMoveLeft;
exports.playerMoveDiagonal = playerMoveDiagonal;
exports.deck = deck;