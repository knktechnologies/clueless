var config = {};

config.port = 3000;

config.minPlayers = 3;
config.maxPlayers = 6;

config.up = -7;
config.right = 1;
config.left = -1;
config.down = 7;
config.diagonals = { '8': 32, '40': -32, '12': 24, '33': -24 }
config.rooms = [8,10,12,22,24,26,36,38,40];

module.exports = config;