var _ = require('underscore');

var chat = {};

function join(gameId, playerId, username) {
    if(!chat[gameId]) {
        chat[gameId] = {};
        chat[gameId].messages = [];
        chat[gameId].players = {};
    }

    if(!chat[gameId].players[playerId]) {
        var date = new Date();
        var message = {
            username: 'system',
            content: username + ' joined the chat',
            posted: date
        };

        chat[gameId].messages.push(message);

        chat[gameId].players[playerId] = { username: username };
    }

    return chat[gameId].messages;
}

function postMessage(gameId, playerId, content) {
    var message = { username: chat[gameId].players[playerId].username,
                    content: content,
                    posted: new Date()
    };
    chat[gameId].messages.push(message);

    return chat[gameId].messages;
}

function postSystemMessage(gameId, content) {
    var message = { username: 'system',
                    content: content,
                    posted: new Date()
    }

    chat[gameId].messages.push(message);

    return chat[gameId].message;
}

function getMessages(gameId) {
    return chat[gameId].messages;
}

exports.join = join;
exports.postMessage = postMessage
exports.postSystemMessage = postSystemMessage;
exports.getMessages = getMessages;
