var express = require('express');
var app = express();
var server = require('http').createServer(app);
var Game = require('./game.js');
var Chat = require('./chat.js');
var io = require('socket.io').listen(server);
var bodyParser = require('body-parser');
var players = {};
var chats = {};

server.listen(process.env.PORT || 3000);

app.set('view engine', 'ejs');
app.set('view options', { layout: false });
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/res', express.static('res'));
app.use('/node_modules', express.static('node_modules'));
app.use('/components', express.static('components'));
app.use('/services', express.static('services'));

function broadcastGame(gameId) {
    broadcastGameBroad(gameId);
    broadcastGameSelective(gameId);
}

function broadcastGameBroad(gameId) {
    var game = Game.getGame(gameId);
    if(!game.isStarted)
        players[gameId][game.players[0].playerId].emit('makeMaster');
    gameSocket.emit('updateGame', game);
}

function broadcastChat(gameId) {
    var messages = Chat.getMessages(gameId);
    for(var chatter in chats[gameId])
        chats[gameId][chatter].emit('updateChat', messages);
}

function broadcastGameSelective(gameId) {
    for(var player in players[gameId])
        players[gameId][player].emit('updatePlayer', Game.getPlayer(gameId,player));
}

var lobbySocket = io
    .of('/lobby')
    .on('connection', function(socket) {
        console.info('lobby socket: initialized (' + socket.id + ')');
        socket.inLobby = true;
        if(players[socket.gameId])
            if(players[socket.gameId][socket.playerId]) {
                console.info('socket exists for combo');
                players[socket.gameId][socket.playerId].emit('disconnect');
            }
        var gameList = Game.list();
        console.info('lobby socket: updating game list')
        socket.emit('updateGameList', gameList);

    });

var chatSocket = io
    .of('/chat')
    .on('connection', function(socket) {
        console.info('chat socket initialized (' + socket.id + ')');
        socket.emit('requestCredentials');
        socket.on('authenticate', function(data) {
            console.info('chat socket authenticating');
            if(players[data.gameId]) {
                if(players[data.gameId][data.playerId]) {
                    console.info('socket exists for combo');
                    if(!chats[data.gameId])
                        chats[data.gameId] = {};

                    chats[data.gameId][data.playerId] = socket;
                    Chat.join(data.gameId, data.playerId, data.username);
                    broadcastChat(data.gameId);
                }
            }
            else
                socket.emit('disconnect');
        });
        socket.on('postMessage', function(data) {
            Chat.postMessage(data.gameId, data.playerId, data.content);
            broadcastChat(data.gameId);
        });
    });

var gameSocket = io
    .of('/game')
    .on('connection', function(socket) {
        console.info('game socket initialized (' + socket.id + ')');
        socket.emit('requestCredentials');
        socket.on('authenticate', function(data) {
            console.log('game socket authenticating');
            var game = Game.getGame(data.gameId);
            if(!players[data.gameId])
                players[data.gameId] = {};
            if(game) {
                console.log('game exists -- setting up socket')
                players[data.gameId][data.playerId] = socket;
                socket.gameId = data.gameId;
                socket.playerId = data.playerId;
                socket.inLobby = false;
                broadcastGameBroad(data.gameId);
            } else {
                console.info('game socket unable to authenticate user for ' + data.gameId);
                socket.emit('gameError', 'authentication error');
            }
        });

        socket.on('startGame', function() {
            console.info('starting game');
            var game = Game.getGame(socket.gameId);
            if(game.players[0].playerId == socket.playerId) { // first player is always master player
                Game.startGame(game);
                broadcastGameBroad(game.gameId);
                lobbySocket.emit('updateGameList', Game.list());
                Chat.postSystemMessage(game.gameId, 'Game has started!');
                broadcastChat(game.gameId);
                broadcastGame(game.gameId);
                var player = game.players[game.currentTurn];
                players[game.gameId][player.playerId].emit('enableTurn', Game.getPlayer(game.gameId, player.playerId));   // enable turn
            }
            else {
                players[socket.gameId][socket.playerId].emit('gameError', 'game starting error');
            }
        });

        socket.on('setNextTurn', function() {
            var game = Game.getGame(socket.gameId);
            Chat.postSystemMessage(socket.gameId, Game.getCharacterName(Game.getPlayer(socket.gameId,socket.playerId).character) + ' has completed a turn');
            players[game.gameId][game.players[game.currentTurn].playerId].emit('disableTurn');  // close out turn
            console.info('current turn is ' + game.currentTurn);
            Game.setNextTurn(game.gameId);
            console.info('next turn is ' + game.currentTurn);
            players[game.gameId][game.players[game.currentTurn].playerId].emit('enableTurn');   // enable turn
            broadcastChat(game.gameId);
            broadcastGameBroad(game.gameId);
        });

        socket.on('passNextTurn', function() {
            var game = Game.getGame(socket.gameId);
            Chat.postSystemMessage(socket.gameId, Game.getCharacterName(Game.getPlayer(socket.gameId,socket.playerId).character) + ' has passed on their turn');
            players[game.gameId][game.players[game.currentTurn].playerId].emit('disableTurn');  // close out turn
            Game.setNextTurn(game.gameId);
            players[game.gameId][game.players[game.currentTurn].playerId].emit('enableTurn');   // enable turn
            broadcastChat(game.gameId);
            broadcastGameBroad(game.gameId);
            if(game.enableDisprove && (game.players[game.currentTurn].playerId == game.suggester)) {
                console.info('accuse only being fire');
                game.enableDisprove = false;
                players[socket.gameId][game.players[game.currentTurn].playerId].emit('accuseOnly');
            }
        });

        socket.on('moveUp', function() {
            if(Game.isRoom(Game.playerMoveUp(socket.gameId, socket.playerId).location)) {
                socket.emit('enableSuggestion');
            }
            else {
                socket.emit('completeTurn');
                broadcastGameSelective(socket.gameId);
            }
            broadcastGame(socket.gameId);
            Chat.postSystemMessage(socket.gameId, Game.getCharacterName(Game.getPlayer(socket.gameId,socket.playerId).character) + ' has moved up');
            broadcastChat(socket.gameId);
        });
        socket.on('moveRight', function() {
            if(Game.isRoom(Game.playerMoveRight(socket.gameId, socket.playerId).location)) {
                socket.emit('enableSuggestion');
            }
            else {
                socket.emit('completeTurn');
                broadcastGameSelective(socket.gameId);
            }
            broadcastGame(socket.gameId);
            Chat.postSystemMessage(socket.gameId, Game.getCharacterName(Game.getPlayer(socket.gameId,socket.playerId).character) + ' has moved right');
            broadcastChat(socket.gameId);
        });
        socket.on('moveLeft', function() {
            if(Game.isRoom(Game.playerMoveLeft(socket.gameId, socket.playerId).location)) {
                socket.emit('enableSuggestion');
            }
            else {
                socket.emit('completeTurn');
            }
            broadcastGame(socket.gameId);
            Chat.postSystemMessage(socket.gameId, Game.getCharacterName(Game.getPlayer(socket.gameId,socket.playerId).character) + ' has moved left');
            broadcastChat(socket.gameId);
        });
        socket.on('moveDown', function() {
            if(Game.isRoom(Game.playerMoveDown(socket.gameId, socket.playerId).location)) {
                socket.emit('enableSuggestion');
            }
            else {
                socket.emit('completeTurn');
                broadcastGameSelective(socket.gameId);
            }
            broadcastGame(socket.gameId);
            Chat.postSystemMessage(socket.gameId, Game.getCharacterName(Game.getPlayer(socket.gameId,socket.playerId).character) + ' has moved down');
            broadcastChat(socket.gameId);
        });
        socket.on('moveDiagonal', function() {
            if(Game.isRoom(Game.playerMoveDiagonal(socket.gameId, socket.playerId).location)) {
                socket.emit('enableSuggestion');
            }
            else {
                socket.emit('completeTurn');
                broadcastGameSelective(socket.gameId);
            }
            broadcastGame(socket.gameId);
            Chat.postSystemMessage(socket.gameId, Game.getCharacterName(Game.getPlayer(socket.gameId,socket.playerId).character) + ' has taken a secret passage');
            broadcastChat(socket.gameId);
        });

        socket.on('makeSuggestion', function(character, room, weapon) {
            var player = Game.moveCharacterToRoom(socket.gameId, character, room);
            console.info(player);
            if(player.playerId)
                players[socket.gameId][player.playerId].emit('enableSpecialSuggestion');
            players[socket.gameId][socket.playerId].emit('disableTurn');  // close out turn
            Chat.postSystemMessage(socket.gameId, Game.getPlayer(socket.gameId,socket.playerId).playerName + ' has suggested: ' + Game.deck[character].name + ', ' + Game.deck[room].name + ', ' + Game.deck[weapon].name);
            var nextPlayer = Game.makeSuggestion(socket.gameId, socket.playerId, character, room, weapon); // broadcast to everyone the suggestion
            players[socket.gameId][nextPlayer.playerId].emit('enableTurn');   // enable turn
            broadcastGame(socket.gameId);
            broadcastChat(socket.gameId);
            // loop through the players until either disproved or back to original
        });

        socket.on('makeAccusation', function(character, room, weapon) {
            var game = Game.getGame(socket.gameId);
            if(Game.makeAccusation(socket.gameId, socket.playerId, character, room, weapon)) {
                Chat.postSystemMessage(socket.gameId, Game.getPlayer(socket.gameId, socket.playerId).playerName + ' has correctly accused: ' + Game.deck[character].name + ', ' + Game.deck[room].name + ', ' + Game.deck[weapon].name);
                players[socket.gameId][socket.playerId].emit('disableTurn');
                Chat.postSystemMessage(socket.gameId, 'The game has ended!');
            }
            else {
                Chat.postSystemMessage(socket.gameId, Game.getPlayer(socket.gameId, socket.playerId).playerName + ' has incorrectly accused: ' + Game.deck[character].name + ', ' + Game.deck[room].name + ', ' + Game.deck[weapon].name);
                Game.getPlayer(socket.gameId, socket.playerId).isActive = false;
                players[socket.gameId][socket.playerId].emit('disableTurn');
                Game.getPlayer(socket.gameId, socket.playerId).location = Game.deck[room].location;
                Game.setNextTurn(socket.gameId);
                players[socket.gameId][game.players[game.currentTurn].playerId].emit('enableTurn');   // enable turn
            }
            broadcastGame(socket.gameId);
            broadcastChat(socket.gameId);
        });

        socket.on('submitDisprove', function(proof) {
            var game = Game.getGame(socket.gameId);
            Chat.postSystemMessage(socket.gameId, Game.getPlayer(socket.gameId,socket.playerId).playerName + ' has disproved the suggestion');
            broadcastChat(socket.gameId);
            players[socket.gameId][game.suggester].emit('showDisprove', proof);
            players[socket.gameId][socket.playerId].emit('disableTurn');  // close out turn
            Game.disprove(socket.gameId,socket.playerId);
            players[socket.gameId][game.suggester].emit('enableTurn');   // enable turn
            players[socket.gameId][game.suggester].emit('accuseOnly');
            broadcastGame(socket.gameId);
        });

        socket.on('disconnect', function() {
            console.info('disconnecting from game socket called');
            if(socket.gameId && socket.playerId && socket.inLobby) {
                console.info('removing from game');
                delete players[socket.gameId][socket.playerId];
                Game.departGame(socket.gameId, socket.playerId);
                broadcastGameBroad(socket.gameId);
                lobbySocket.emit('updateGameList', Game.list());
            }
        });

        socket.on('quit', function() {
            if(socket.gameId && socket.playerId) {
                socket.emit('quit');
                Chat.postSystemMessage(socket.gameId, Game.getPlayer(socket.gameId,socket.playerId).playerName + ' has left the game');
                broadcastChat(socket.gameId);
                Game.departGame(socket.gameId, socket.playerId);
                var game = Game.getGame(socket.gameId);
                if(game) {
                    broadcastGameBroad(socket.gameId);
                    players[socket.gameId][game.players[game.currentTurn].playerId].emit('enableTurn');   // enable turn
                }
                delete players[socket.gameId][socket.playerId];
                lobbySocket.emit('updateGameList', Game.list());
            }
        });
    });

app.get('/', function (req, res) {  res.sendFile('index.html', { root: './' }); });
app.get('/success', function (req, res) {  res.sendFile('success.html', { root: './' }); });
app.get('/help', function (req, res) {  res.sendFile('help.html', { root: './' }); });
app.get('/rules', function (req, res) {  res.sendFile('rules.html', { root: './' }); });
app.get('/error', function (req, res) {  res.sendFile('error.html', { root: './' }); });
app.get('/list', function (req, res) { console.info('list called'); res.json(Game.list()); });

app.post('/add', function (req, res) {
    console.info('adding game: ' + req);
    var NewGame = Game.addGame(req.body);
    res.json(NewGame);
    lobbySocket.emit('updateGames', Game.list());
});

app.post('/joinGame', function (req, res) {
    console.info('joining game');
    var game = Game.getGame(req.body.gameId);
    res.json(Game.joinGame(game, req.body.player));
    lobbySocket.emit('updateGameList', Game.list());
});

app.post('/setNextTurn', function(req, res){
    Game.setNextTurn(req.body.gameId, req.body.playerId);
    broadcastChat(req.body.gameId); // announce turn change to players
    broadcastGame(req.body.gameId);
    returnGame(req.body.gameId, res);
});