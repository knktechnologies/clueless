'use strict';

angular
    .module('system.services', [])
    .factory('AlertService', function() {
        var AlertService = {};

        var base = ["<div class=\"alert alert-", "\">", "<strong> ", "</strong> ", "</div>"];
        var successGlyphicon = "<span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>";
        var infoGlyphicon = "<span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span>";
        var warningGlyphicon = "<span class=\"glyphicon glyphicon-alert\" aria-hidden=\"true\"></span>";
        var dangerGlyphicon = "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>";
        var createAlert = function(tag, alert, message, adds) {
            return base[0] + tag + base[1] + adds + base[2] + alert + base[3] + message + base[4];
        };

        AlertService.success = function(alert, message) {
            return createAlert("success", alert, message, successGlyphicon);
        };
        AlertService.info = function(alert, message) {
            return createAlert("info", alert, message, infoGlyphicon);
        };
        AlertService.warning = function(alert, message) {
            return createAlert("warning", alert, message, warningGlyphicon);
        };
        AlertService.danger = function(alert, message) {
            return createAlert("danger", alert, message, dangerGlyphicon);
        };

        return AlertService;
    });
