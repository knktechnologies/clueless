'use strict';

angular
    .module('system.services')
    .factory('AuthService', ['$auth', '$http', function($auth, $http, $authProvider) {
        var AuthService = {};

        // default login
        var username = 'admin';
        var password = '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8';
        /* spare usernames */
        var temp_user1 = 'John';
        var temp_user2 = 'Jim';
        var temp_user3 = 'Joe';

        var authenticatorUrl = 'https://www.googleapis.com/plus/v1/people/me';

        var authenticated = false; // only needed for username/password login
        AuthService.login = function(input_username, input_password) {
            if((input_username == username) && (input_password ==  password))
                return true;

            else if((input_username == temp_user1) && (input_password ==  password))
                return true;

            else if((input_username == temp_user2) && (input_password ==  password))
                return true;

            else if((input_username == temp_user3) && (input_password ==  password))
                return true;

            return false;
        };
        AuthService.setAuthentication = function() {
            authenticated = true;
        };
        AuthService.cutAuthentication = function() {
            authenticated = false;
        };
        AuthService.authenticate = function(provider) {
            return $auth.authenticate(provider);
        };
        AuthService.isAuthenticated = function() {
            return ($auth.isAuthenticated() || authenticated);
        };
        AuthService.logout = function() {
            AuthService.cutAuthentication();
            $auth.logout();
        };
        AuthService.getUsername = function() {
            return $http.get(authenticatorUrl);
        }

        return AuthService;
    }]);