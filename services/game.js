'use strict';

angular
    .module('system.services')
    .factory('GameService', ['$http', 'DisprovalFilter', function($http, DisprovalFilter) {
        console.log('GameService fired');
        var GameService = {};

        var createGameId = function() {
            var start = 0;
            var end = 10;
            return sha256(Math.random().toString()).slice(start,end)
        };
        var createMatrix = function() { // creates the adjacency matrix for the board
            var matrix = new Array(49).fill(new Array(49));

            //-- Character Starting Positions --//
            matrix[4][11] = true;  // Miss Scarlett
            matrix[14][15] = true; // Professor Plum
            matrix[20][19] = true; // Colonel Mustard
            matrix[28][29] = true; // Mrs. Peacock
            matrix[44][37] = true; // Mr. Green
            matrix[46][39] = true; // Mrs. White

            // Regular Rooms
            matrix[10][9] = true;  // Hall -> left
            matrix[10][11] = true; // Hall -> right
            matrix[10][17] = true; // Hall -> down
            matrix[22][15] = true; // Library -> up
            matrix[22][23] = true; // Library -> right
            matrix[22][29] = true; // Library -> down
            matrix[24][17] = true; // Billiard Room -> up
            matrix[24][23] = true; // Billiard Room -> left
            matrix[24][25] = true; // Billiard Room -> right
            matrix[24][31] = true; // Billiard Room -> down
            matrix[26][19] = true; // Dining Room -> up
            matrix[26][25] = true; // Dining Room -> left
            matrix[26][33] = true; // Dining Room -> down
            matrix[38][31] = true; // Ballroom -> up
            matrix[38][37] = true; // Ballroom -> left
            matrix[38][39] = true; // Ballroom -> right

            // Corner Rooms (with secret passageways)
            matrix[8][9] = true;   // Study -> right
            matrix[8][15] = true;  // Study -> down
            matrix[8][40] = true;  // Study -> secret
            matrix[12][11] = true; // Lounge -> left
            matrix[12][19] = true; // Lounge -> down
            matrix[12][36] = true; // Lounge -> secret
            matrix[36][12] = true; // Conservatory -> secret
            matrix[36][29] = true; // Conservatory -> up
            matrix[36][37] = true; // Conservatory -> right
            matrix[40][8] = true;  // Kitchen -> secret
            matrix[40][33] = true; // Kitchen -> up
            matrix[40][39] = true; // Kitchen -> left

            // halls
            matrix[9][8] = true;   // Hallway -> left
            matrix[9][10] = true;  // Hallway -> right
            matrix[11][10] = true; // Hallway -> left
            matrix[11][12] = true; // Hallway -> right
            matrix[15][8] = true;  // Hallway -> up
            matrix[15][22] = true; // Hallway -> down
            matrix[17][10] = true; // Hallway -> up
            matrix[17][24] = true; // Hallway -> down
            matrix[19][12] = true; // Hallway -> up
            matrix[19][26] = true; // Hallway -> down
            matrix[23][22] = true; // Hallway -> left
            matrix[23][24] = true; // Hallway -> right
            matrix[25][24] = true; // Hallway -> left
            matrix[25][26] = true; // Hallway -> right
            matrix[29][22] = true; // Hallway -> up
            matrix[29][36] = true; // Hallway -> down
            matrix[31][24] = true; // Hallway -> up
            matrix[31][38] = true; // Hallway -> down
            matrix[33][26] = true; // Hallway -> up
            matrix[33][40] = true; // Hallway -> down
            matrix[37][36] = true; // Hallway -> left
            matrix[37][38] = true; // Hallway -> right
            matrix[39][38] = true; // Hallway -> left
            matrix[39][40] = true; // Hallway -> right

            return matrix;
        };
        var createPlayerId = function(playerName) {
            var start = 0;
            var end = 10;
            //return sha256(playerName).slice(start,end); // should not do this while in development -- more difficult to test with same login
            return sha256(Math.random().toString()).slice(start,end)
        };
        var adjacencyMatrix = createMatrix();

        var cluesFile = 'res/data/clues.json';

        GameService.createPlayer = function(username) {
            return {    playerName: username,
                        playerId:   createPlayerId(username)    }
        };
        GameService.getGames = function() {
            console.log('getting game list');
            return $http.get('/list');
        };
        GameService.createGame = function(gameName) {
            var gameId = createGameId();
            console.log('creating game: ' + gameName + ' (' + gameId + ')');
            return $http.post('/add', { gameId: gameId, gameName: gameName });
        };
        GameService.joinGame = function(gameId, playerName) {
            var player = GameService.createPlayer(playerName);
            console.log('joining game: ' + playerName + ' joining ' + gameId);
            return $http.post("/joinGame", { gameId: gameId, player: player });
        };
        GameService.startGame = function(gameId) {
            return $http.post("/startgame", { gameId: gameId });
        };
        GameService.departGame = function(gameId, playerId) {
            $http.post('/departgame', { gameId: gameId, playerId: playerId});
        };

        GameService.getClues = function() {
            return $http.get(cluesFile);
        };
        GameService.createGameboard = function(data) {
            var gameboard = {};
            gameboard.locations = [49];
            data.rooms.forEach(function(room){
                gameboard.locations[room.location] = {};
                gameboard.locations[room.location].style = { 'background-image': 'url("' + room.imageUrl + '")' }
            });
            gameboard.characters = data.characters;
            gameboard.rooms = data.rooms;
            gameboard.weapons = data.weapons;
            gameboard.deck = data.rooms.concat(data.characters.concat(data.weapons));

            return gameboard;
        };
        GameService.isRoom = function(position) {
            if(location % 2 == 0)
                if (((location-1)%7==0) || ((location+2)%7==0) || (location-3)%7==0)
                    return true;
            return false;
        };
        GameService.isHall = function(position) {
            var rooms = [8,10,12,22,24,26,36,38,40];
            var hall = true;
            rooms.forEach(function(room) {
                if(room == position) {
                    console.log ('room ' + room + ' equals ' + position);
                    hall = false;
                }
            });
            return hall;
        };
        GameService.isAdjacent = function(origin, target) {
            if(target > 48 || target < 0)
                return false;
            return adjacencyMatrix[origin][target];
        };
        GameService.getMoves = function(location) {
            var move = new Array(5);
            if(GameService.isAdjacent(location,location-7))
                move[0] = true;
            if(GameService.isAdjacent(location, location+1))
                move[1] = true;
            if(GameService.isAdjacent(location, location+7))
                move[2] = true;
            if(GameService.isAdjacent(location, location-1))
                move[3] = true;
            if(location==8 || location==12 || location==36 || location==40)
                move[4] = true;
            return move;
        };
        GameService.getDisprovalCards = function(cards, suggestion) {
            return DisprovalFilter(cards, suggestion);
        };

        return GameService;
    }])
    .filter('Disproval', function() {
        console.log('Disproval Filter fired');
        return function(cards, suggestion) {
            var out = [];
            cards.forEach(function(card) {
                if(card == suggestion.character)
                    out.push(card);
                else if(card == suggestion.room)
                    out.push(card);
                else if(card == suggestion.weapon)
                    out.push(card);
            });
            return out;
        }
    });
